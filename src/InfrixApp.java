import java.util.Scanner;
public class InfrixApp {

    public static void main(String[] args) {
        String input, output;
        while(true) {
            Scanner kb = new Scanner(System.in);
            System.out.print("Enter infix: ");
            input = kb.nextLine();
            if(input.equals("")) break;
            IntoPost theTrans = new IntoPost(input);
            output = theTrans.doTrans();
            System.out.println("Postfix is " + output + '\n');
        }
    }
}
